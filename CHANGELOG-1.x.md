# Release Notes for 1.x

    2020-03-10 v0.2.1: Manage Changelog file
    2020-02-05 v0.2.0: Add laravel backup panel
    2020-02-05 v0.1.2: Remove config
    2020-02-05 v0.1.1: Remove not related class
    2020-01-06 v0.1.0: Add config
    2019-12-26 v0.0.1: Initial pre release