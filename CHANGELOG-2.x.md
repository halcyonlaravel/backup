# Release Notes for 2.x

    2021-05-18 v2.2.0-alpha1 Fix Discord 
    2021-05-18 v2.2.0-alpha use dev-l8-compatibility in laravel-backup-panel
    2021-04-23 v2.1.2 require composer v2 on dev
    2021-04-23 v2.1.1 Add support for PHP8, upgrade some packages
    2021-01-26 v2.1.0 Use interface for gate
    2021-01-25 v2.0.1 Upgrade pavel-mironchik/laravel-backup-panel to v2
    2020-03-25 v2.0.0 Use stable version of laravel-notification-channels/discord
    2020-03-10 v2.0.0-alpha1: Prepare for Laravel 7