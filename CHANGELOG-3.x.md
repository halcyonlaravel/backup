# Release Notes for 3.x

    2022-05-16 v3.1.1 Use pavel-mironchik/laravel-backup-panel ^2.2
    2022-04-20 v3.1.0 Add support laravel ^9.0, spatie/laravel-backuo ^8.1.2, use fork PR laravel-backup-panel
    2021-05-21 v3.0.1 Remove laravel-notification-channels/discord
    2021-05-19 v3.0.0 Use Stable laravel-backup-panel ^2.1.1
    2021-05-18 v3.0.0-alpha1 Cannot solve discord notification, 
                so using spatie/laravel-backup ^7.5.2 will be solve, its already support discord notification
    2021-05-18 v3.0.0-alpha 
                Fix Discord, 
                drop support spatie/laravel-backup v6 and set to 7.3.*
                drop support laravel 6 and 7
                drop suppor php 7