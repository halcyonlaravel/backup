<?php

namespace HalcyonLaravelBoilerplate\Backup\Contracts;

interface UserAccessBackUpInterface
{
    public function canAccessBackup(): bool;
}