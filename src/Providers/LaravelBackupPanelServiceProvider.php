<?php

namespace HalcyonLaravelBoilerplate\Backup\Providers;

use HalcyonLaravelBoilerplate\Backup\Contracts\UserAccessBackUpInterface;
use Illuminate\Support\Facades\Gate;
use PavelMironchik\LaravelBackupPanel\LaravelBackupPanelApplicationServiceProvider;

class LaravelBackupPanelServiceProvider extends LaravelBackupPanelApplicationServiceProvider
{
    /**
     * Register the Laravel Backup Panel gate.
     *
     * This gate determines who can access Laravel Backup Panel in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define(
            'viewLaravelBackupPanel',
            function (UserAccessBackUpInterface $user) {
                return $user->canAccessBackup();
            }
        );
    }
}